from django.db import models

# Create your models here.
class Pokemon(models.Model):
    naam = models.CharField(max_length=30, default="", blank=False)
    soort = models.CharField(max_length=30, default="", blank=False)
    kleur = models.CharField(max_length=30, default="onbekend", choices = 
        [
            ('onbekend','Onbekend'),
            ('geel','Geel'),
            ('oranje','Oranje'),
            ('blauw','Blauw'),
            ('Groen','Groen')
        ]
    )
    sterkte = models.IntegerField(blank=True)
    class Meta:
        verbose_name_plural = "Pokemon's"
    def __str(self):
        return self.naam+"-"+str(self.sterkte)