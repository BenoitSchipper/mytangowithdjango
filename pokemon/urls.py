from django.urls import include, path
from . import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()
router.register(r'api/v1/pokemon', views.PokemonModelViewset)

urlpatterns = [
    path('', include(router.urls)),
]