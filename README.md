# MyTangoWithDjango

- A working Django deployment example. 
- Clone it and run three commands to create a model, migrate and run the django api server.
- This was my first proof of concept using the djangorestframework.

Prerequisites
------------

You wil need the following packages:

- python3
- pip3
- Django (via `pip3 install django`)
- Django Restframework (via `pip3 install djangorestframework`)

Requirements
------------

Once you have cloned the repository do the following wthin the repositiry folder:

- Make a model as follows

`python ./manage.py makemigrations pokemon`

- Apply the migrations as follows

`python ./manage.py migrate`

- Run the server as follows

`python ./manage.py runserver`

- Go to http://127.0.0.1:8000/ for the root api
- Go to http://127.0.0.1:8000/api/v1/pokemon/ to view the pokemon (example) api


License
-------

BSD

Benoit Schipper
------------------

https://gitlab.com/BenoitSchipper

Django Information
------------------
                      
- https://www.djangoproject.com/
- https://www.djangoproject.com/start/
- https://docs.djangoproject.com/en/3.1/intro/tutorial01/

